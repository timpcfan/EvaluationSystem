package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.User;
import table.UserHandle;

/**
 * 用于修改用户信息的控制器
 * 使用页面：admin_user_manage.jsp
 */
@WebServlet("/ModifyUser")
public class ModifyUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyUser() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		String old_userid = Helper.getString(request.getParameter("old_userid"));
		User muser = new User();
		String id = Helper.getString(request.getParameter("userid"));
		String name = Helper.getString(request.getParameter("user_name"));
		String steamid =Helper.getString(request.getParameter("teamid"));
		String password = Helper.getString(request.getParameter("reset_password"));
		
		// 判断输入合法性
		if(!id.matches("^[0-9]+$")) {
			response.getWriter().println("<script>window.location.href='admin_user_manage.jsp?userid=" + old_userid + "';alert('userid should be number!');</script>");
			return;
		}
		if(id.length() > 20) {
			response.getWriter().println("<script>window.location.href='admin_user_manage.jsp?userid=" + old_userid + "';alert('userid too long!');</script>");
			return;
		}
		if(!steamid.matches("^[0-9]+$")) {
			response.getWriter().println("<script>window.location.href='admin_user_manage.jsp?userid=" + old_userid + "';alert('teamid should be number(1~32766)!');</script>");
			return;
		}
		int teamid = Integer.parseInt(steamid);
		if(teamid < 0 || teamid >= 32767) {
			response.getWriter().println("<script>window.location.href='admin_user_manage.jsp?userid=" + old_userid + "';alert('teamid should be number(1~32766)!');</script>");
			return;
		}
		
		muser.setId(id);
		muser.setName(name);
		muser.setPassword(password);
		muser.setTeamid(teamid);
		
		UserHandle handle = new UserHandle();
		if(handle.updateToDB(old_userid, muser)) {
			// 如果修改成功则返回主页面，并弹出修改成功
			response.getWriter().println("<script>window.location.href='admin_user_manage_main.jsp" + "';alert('Successfully modified!');</script>");			
		}else {
			// 如果修改失败则返回之前修改页面，并弹出修改失败
			response.getWriter().println("<script>window.location.href='admin_user_manage.jsp?userid=" + old_userid + "';alert('Fail to modify!');</script>");
		}
	}

}
