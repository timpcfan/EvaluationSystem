package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import table.*;

/**
 * 添加一个id为0的空小组进入数据库
 * 无参数
 */
@WebServlet("/AddTeam")
public class AddTeam extends HttpServlet{
	
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	    */
	    public AddTeam() {
	        super();
	    }

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
			// 管理员登录有效性验证段
			User user = (User) request.getSession().getAttribute("user");
			if(user == null || !user.getGroup().equals("admin")){
				request.getRequestDispatcher("index.jsp").forward(request, response);
				return;
			}
			
			// 响应设置段
			response.setContentType("text/xml; charset=UTF-8");
			response.setHeader("Cache-Control","no-store");
			response.setHeader("Pragma","no-cache");
			response.setDateHeader("Expires",0);
			
			
			TeamHandle handle = new TeamHandle();
			Team team = new Team();
			team.setName("[新小组]");
			if(handle.addTeam(team)) {
				response.getWriter().print("<root><result>OK</result></root>");
			}else {
				response.getWriter().print("<root><result>FAIL</result></root>");
			}
			response.getWriter().flush();
		}

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doGet(request, response);
		}

	}


