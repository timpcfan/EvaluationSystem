package action;

import java.io.IOException;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.EvalHandle;
import table.EvaluationEvent;
import table.User;

/**
 * 用于改变评价活动结束时间的控制器
 */
@WebServlet("/ChangeEndtime")
public class ChangeEndtime extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeEndtime() {
        super();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
  }
    /**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || !user.getGroup().equals("admin")) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		int eeid=0;
		try{
			eeid = Integer.valueOf(request.getParameter("eeid"));
		}catch(Exception e){
			response.getWriter().print("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Error eeid!');</script>");
			return;
		}
		EvalHandle handle = new EvalHandle();
		EvaluationEvent event = handle.getEeById(eeid);
		String HTMLendtime = Helper.getString(request.getParameter("end_time"));
		System.out.println(HTMLendtime);
		if (HTMLendtime == null || HTMLendtime.isEmpty()) {
			response.getWriter().print("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Please enter information of endtime!');</script>");
			return;
		}
			Date endtime = Helper.parseTimeString(HTMLendtime);
			event.setEndTime(Helper.dateToDBTime(endtime));
			handle.updateEeToDB(eeid, event);
			response.getWriter().println("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Successfully modified!');</script>");
	}
		}

