package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import common.Helper;
import table.User;
import table.UserHandle;

/**
 * 管理用户登录的控制器
 * 使用页面：index.jsp
 */
@WebServlet("/LoginCheck")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginCheck() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 通过get方法访问该servlet无效，跳转回登录界面
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = new User();
		
		String txtUserId = Helper.getString(request.getParameter("txt_username"));
		String txtUserPassword = Helper.getString(request.getParameter("txt_password"));
		String userType = Helper.getString(request.getParameter("user_type"));
		
	    System.out.println("userid:" + txtUserId);
	    System.out.println("userpw:" + txtUserPassword);
	    System.out.println("type:" + userType);
		
		
		user.setId(txtUserId);
		user.setPassword(txtUserPassword);
		user.setGroup(userType);
		
		UserHandle handle = new UserHandle();
		
		// 检测用户合法性
		int stat = handle.CheckUser(user);
		if(stat == -2) { 
			System.out.println("login err! (password err)");
			response.getWriter().println("<script>window.location.href='index.jsp"+ "';alert('WRONG ID or PASSWORD!');</script>");
			return;
		}
		if(stat == -1) {
			System.out.println("login err! (authority err)");
			response.getWriter().println("<script>window.location.href='index.jsp"+ "';alert('You are NOT admin!');</script>");
			return;
		}
		System.out.println("login successfully!");
		
		handle.updateInfoById(user); // 补全用户信息
		
		// 将user加入session
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		session.setMaxInactiveInterval(-1);
		
		
		// 选择用户模式
		switch(userType) {
		case "admin":
			request.getRequestDispatcher("admin_main.jsp").forward(request, response);
			break;
		case "user":
			request.getRequestDispatcher("user_main.jsp").forward(request, response);
			break;
		case "guest":
			user = new User();
			user.setName("访客");
			user.setGroup("guest");
			session.setAttribute("user", user);
			request.getRequestDispatcher("user_main.jsp").forward(request, response);
			break;
		}
	}

}
