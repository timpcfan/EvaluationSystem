package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.EvalHandle;
import table.User;

/**
 * 用于删除指定评价的控制器
 * 参数：evalid 要被删除的评价id
 */
@WebServlet("/DeleteEval")
public class DeleteEval extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteEval() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// 响应设置段
		response.setContentType("text/xml; charset=UTF-8");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Pragma","no-cache");
		response.setDateHeader("Expires",0);
		
		String sevalid = Helper.getString(request.getParameter("evalid"));
		int evalid = 0;
		try {
			evalid = Integer.parseInt(sevalid);
		}catch(NumberFormatException e) {
			response.getWriter().println("<root><result>FAIL</result></root>");
			return;
		}
		
		EvalHandle h = new EvalHandle();
		if(h.deleteEvalById(evalid)) {
			response.getWriter().println("<root><result>OK</result></root>");
		} else {
			response.getWriter().println("<root><result>FAIL</result></root>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
