package action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.User;
import table.UserHandle;

/**
 * 批量添加用户使用的控制器
 * 使用页面：admin_user_add.jsp
 */
@WebServlet("/AddUsers")
public class AddUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUsers() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print("Please use POST method!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		// TODO 用户编号合法性， 用户姓名合法性检测
		String s_num = Helper.getString(request.getParameter("num_of_items"));
		int num_of_items = 0;
		try {
			num_of_items = Integer.parseInt(s_num);
		}catch(NumberFormatException e) {
			response.getWriter().println("<script>window.history.back(-1);alert('Error!');</script>");
			return;
		}
		String mode = Helper.getString(request.getParameter("add_mode"));
		if(!"safe".equals(mode) && !"normal".equals(mode) && !"force".equals(mode)) {
			response.getWriter().println("<script>window.history.back(-1);alert('Mode parameter error!');</script>");
			return;
		}
		
		
		ArrayList<User> list = new ArrayList<>();
		for(int i=0; i<num_of_items; i++) {
			User tuser = new User();
			String tmpid = Helper.getString(request.getParameter("txt_userid_" + i));
			if(tmpid == null || "".equals(tmpid)) {
				continue;
			}
			String tmpname = Helper.getString(request.getParameter("txt_username_" + i));
			String tmppasswd = Helper.getString(request.getParameter("txt_password_" + i));
			String tmpteamid = Helper.getString(request.getParameter("txt_teamid_" + i));
			
			tuser.setId(tmpid);
			tuser.setName(tmpname);
			tuser.setPassword(tmppasswd);
			if(tmpteamid == null || "".equals(tmpteamid)) {
				tuser.setTeamid(0);
			} else {
				tuser.setTeamid(Integer.parseInt(tmpteamid));
			}
			list.add(tuser);
		}
		
		UserHandle handle = new UserHandle();
		int n = handle.addUsers(list, mode);
		if(n==0) {
			response.getWriter().println("<script>window.history.back(-1);alert('Duplicate id key!');</script>");			
		}else {
			response.getWriter().println("<script>window.location.href='admin_user_manage_main.jsp';alert('Successfully add "+ n + " users!')</script>");
		}
	}

}
