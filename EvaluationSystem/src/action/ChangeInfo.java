package action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.EvalHandle;
import table.EvaluationEvent;
import table.User;

/**
 * 用于改变评价活动信息的控制器
 */
@WebServlet("/ChangeInfo")
public class ChangeInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeInfo() {
        super();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
    }

    /**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		String tinfo = Helper.getString(request.getParameter("team_info"));
		int eeid=0;
		try{
			eeid = Integer.valueOf(request.getParameter("eeid"));
		}catch(Exception e){}
		EvalHandle ehandle = new EvalHandle();
		EvaluationEvent ee = ehandle.getEeById(eeid);
		if(tinfo==null||tinfo.isEmpty())
		{
			response.getWriter().println("<script>window.location.href='admin_ee_manage.jsp?userid=" + eeid + "';alert('Please enter information of info!');</script>");
			return;
		}
        ee.setInfo(tinfo);
        ehandle.updateEeToDB(eeid, ee);
		response.getWriter().println("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Successfully modified!');</script>");
	}
}

