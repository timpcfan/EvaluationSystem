package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.*;

/**
 * 更新评价中小组信息的控制器
 */
@WebServlet("/UserTeamSelfEvaluate")
public class UserTeamSelfEvaluate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserTeamSelfEvaluate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 用户登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		String workName = Helper.getString(request.getParameter("txt_team_ename"));
		String workIntro = Helper.getString(request.getParameter("txt_team_production_introduce"));
		String selfEvaluate = Helper.getString(request.getParameter("txt_team_self_evaluate"));
		int	eeid = Integer.parseInt(request.getParameter("eeid"));
		int	teamid = user.getTeamid();
		
		Attendant att = new Attendant();
		att.setEeid(eeid);
		att.setTeamid(teamid);
		att.setEname(workName);
		att.setEinfo(workIntro);
		att.setSelfeval(selfEvaluate);
		
		EvalHandle handle = new EvalHandle();
		if(handle.updateAttendantToDB(eeid, teamid, att)) {
			response.getWriter().println("<script>window.location.href='user_evaluate.jsp';alert('Successfully modified!');</script>");
		}else {
			response.getWriter().println("<script>window.location.href='user_team_info_change.jsp?eeid=" + eeid + "';alert('Fail to modified!');</script>");
		}
	}

}
