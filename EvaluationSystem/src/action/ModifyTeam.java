package action;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import common.Helper;
import table.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.List;

/**
 * 用于修改小组信息的控制器
 * 使用页面：admin_team_manage.jsp
 */
@WebServlet("/ModifyTeam")
public class ModifyTeam extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyTeam() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		int old_teamid = 0;
		try{
			old_teamid = Integer.parseInt(Helper.getString(request.getParameter("old_teamid"))) ;
		}catch(Exception e) {}
		Team mteam = new Team();
		String sid = Helper.getString(request.getParameter("teamid"));
		String name = Helper.getString(request.getParameter("team_name"));
		String info = Helper.getString(request.getParameter("team_info"));

		// 判断输入合法性
		if(!sid.matches("^[0-9]+$")) {
			response.getWriter().println("<script>window.location.href='admin_team_manage.jsp?teamid=" + old_teamid + "';alert('teamid should be number(1~32766)!');</script>");
			return;
		}
		int id = Integer.parseInt(sid);
		if(id <= 0 || id >= 32767) {
			response.getWriter().println("<script>window.location.href='admin_team_manage.jsp?teamid=" + old_teamid + "';alert('teamid should be number(1~32766)!');</script>");
			return;
		}
		
		TeamHandle handle = new TeamHandle();
		List<Team> teams = handle.getAllTeams();
		
		for(Team t:teams) {
			if(t.getId()==id&&id!=old_teamid) {
				response.getWriter().println("<script>window.location.href='admin_team_manage.jsp?teamid=" + old_teamid + "';alert('The id is duplicated! Please change the id and try again!');</script>");
				return;
			}
		}
		Pattern p=Pattern.compile(".*\\PL.*");
		Matcher m=p.matcher(name);
		if(m.find()) {
			response.getWriter().println("<script>window.location.href='admin_team_manage.jsp?teamid=" + old_teamid + "';alert('There can NOT be any punctuation character in the team name!');</script>");
			return;
		}
		mteam.setId(id);
		mteam.setName(name);
		mteam.setInfo(info);
		
		if(handle.updateToDB(old_teamid, mteam)) {
			// 如果修改成功则返回之前修改页面，并弹出修改成功
			response.getWriter().println("<script>window.location.href='admin_team_manage.jsp?teamid=" + mteam.getId() + "';alert('Successfully modified!');</script>");			
		}else {
			// 如果修改失败则返回之前修改页面，并弹出修改失败
			response.getWriter().println("<script>window.history.back(-1);alert('Fail to modify!');</script>");
		}
	}
}
