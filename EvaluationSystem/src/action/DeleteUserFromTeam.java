package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import table.User;
import table.UserHandle;

/**
 * 从小组中删除用户（将用户的teamid字段删除）
 * 参数：userid
 * 返回xml文件，成功则result标签为OK，否则result标签为FAIL
 */
@WebServlet("/DeleteUserFromTeam")
public class DeleteUserFromTeam extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUserFromTeam() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		// 响应设置段
		response.setContentType("text/xml; charset=UTF-8");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Pragma","no-cache");
		response.setDateHeader("Expires",0);
		// 参数获取
		String userid = request.getParameter("userid");
		
		UserHandle handle = new UserHandle();
		User nuser = handle.getUserById(userid); // 得到对应的user
		nuser.setTeamid(0); // 变更小组id为0表示没有小组
		if(handle.updateToDB(nuser.getId(), nuser)) { // 更新数据库信息
			response.getWriter().print("<root><result>OK</result></root>");
		} else {
			response.getWriter().print("<root><result>FAIL</result></root>");
		}
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
