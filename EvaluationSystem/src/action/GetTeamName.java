package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import table.Team;
import table.TeamHandle;
import table.User;

/**
 * 通过传递参数teamid获取小组名的控制器
 * 使用页面：普遍适用
 * 效果：传入teamid参数，将返回对应的小组名称的xml（用于AJAX）
 */
@WebServlet("/GetTeamName")
public class GetTeamName extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetTeamName() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// 响应设置段
		response.setContentType("text/xml; charset=UTF-8");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Pragma","no-cache");
		response.setDateHeader("Expires",0);
		
		String steamid = request.getParameter("teamid");
		if(steamid == null || "".equals(steamid)) {
			return;
		}
		
		String no = request.getParameter("no");
		
		int teamid = Integer.valueOf(steamid);
		TeamHandle handle = new TeamHandle();
		Team team = handle.getTeamById(teamid);
		String name = team.getName();
		response.getWriter().print("<root>");
		response.getWriter().print("<teamname>" + name + "</teamname>");
		if(no != null) { // 如果传入no参数直接写回
			response.getWriter().print("<no>" + no + "</no>");
		}
		response.getWriter().print("</root>");
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
