package action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Helper;
import table.User;
import table.UserHandle;

/**
 * 用于改变用户信息的控制器
 */
@WebServlet("/SelfModifyInfo")
public class SelfModifyInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelfModifyInfo() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 用户登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		UserHandle uh = new UserHandle();
		String old_id = user.getId();
		if(old_id == null) return;
		user = uh.getUserById(old_id);
		if(user == null) return;
		
		String id = Helper.getString(request.getParameter("txt_account"));
		String name = Helper.getString(request.getParameter("txt_name"));
		String steamid = Helper.getString(request.getParameter("txt_team"));
		
		// 判断输入合法性
		if(!id.matches("^[0-9]+$")) {
			response.getWriter().println("<script>window.location.href='user_self_info_modify.jsp';alert('userid should be number!');</script>");
			return;
		}
		if(id.length() > 20) {
			response.getWriter().println("<script>window.location.href='user_self_info_modify.jsp';alert('userid too long!');</script>");
			return;
		}
		user.setId(id);
		user.setName(name);
		if(user.getTeamid() == 0 && !steamid.equals("")) {
			if(!steamid.matches("^[0-9]+$")) {
				response.getWriter().println("<script>window.location.href='user_self_info_modify.jsp';alert('teamid should be number(1~32766)!');</script>");
				return;
			}
			int teamid = 0;
			try { teamid = Integer.parseInt(steamid); } catch(Exception e) {}
			if(teamid <= 0 || teamid >= 32767) {
				response.getWriter().println("<script>window.location.href='user_self_info_modify.jsp';alert('teamid should be number(1~32766)!');</script>");
				return;
			}
			user.setTeamid(teamid);
		}

		if(uh.updateToDB(old_id, user)) {
			// 如果修改成功则返回之前修改页面，并弹出修改成功
			request.getSession().setAttribute("user", user); // 改变session中的user为修改后的user
			response.getWriter().println("<script>window.location.href='user_self_info.jsp';alert('Successfully modified!');</script>");
		}else {
			// 如果修改失败则返回之前修改页面，并弹出修改失败
			response.getWriter().println("<script>window.location.href='user_self_info.jsp';alert('Fail to modify!');</script>");
		}
	}

}
