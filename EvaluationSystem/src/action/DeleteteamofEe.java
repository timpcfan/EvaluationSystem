package action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import table.EvalHandle;
import table.User;

/**
 * 用于删除参与评价活动的小组的控制器
 */
@WebServlet("/DeleteteamofEe")
public class DeleteteamofEe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteteamofEe() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
				User user = (User) request.getSession().getAttribute("user");
				if(user == null || !user.getGroup().equals("admin")){
					request.getRequestDispatcher("index.jsp").forward(request, response);
					return;
				}
				
				// 响应设置段
				response.setContentType("text/xml; charset=UTF-8");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				
				
				String seeid = request.getParameter("eeid");
				if(seeid == null || seeid.isEmpty()) {
					response.getWriter().print("<root><result>wrong parameter</result></root>");
					return;
				}
				int eeid = 0;
				try {
					eeid = Integer.parseInt(seeid);
				} catch (Exception e) {
					response.getWriter().print("<root><result>wrong parameter</result></root>");
					return;
				}
				String seeteamid = request.getParameter("teamid");
				if(seeteamid == null || seeteamid.isEmpty()) {
					response.getWriter().print("<root><result>wrong parameter</result></root>");
					return;
				}
				int teamid = 0;
				try {
					teamid = Integer.parseInt(seeteamid);
				} catch (Exception e) {
					response.getWriter().print("<root><result>wrong parameter</result></root>");
					return;
				}
				EvalHandle handle = new EvalHandle();
				if(handle.deleteAttendant(eeid,teamid)) {
					response.getWriter().print("<root><result>OK</result></root>");
				}else {
					response.getWriter().print("<root><result>FAIL</result></root>");
				}
				response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

