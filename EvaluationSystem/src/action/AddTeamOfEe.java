package action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import common.Helper;
import table.EvalHandle;
import table.User;

/**
 * 用于向评价活动添加成员
 */
@WebServlet("/AddTeamOfEe")
public class AddTeamOfEe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddTeamOfEe() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 使用get访问该页无效
		response.getWriter().append("Please use POST method!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 管理员登录有效性验证段
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || !user.getGroup().equals("admin")){
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		String seeid = request.getParameter("eeid");
		int eeid = 0;
		try {
			eeid = Integer.parseInt(seeid);
		} catch (Exception e) {
			response.getWriter().print("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Error eeid!');</script>");
			return;
		}
		String steamid = Helper.getString(request.getParameter("add_team"));
		int teamid = 0;
		try {
			teamid = Integer.parseInt(steamid);
		} catch (Exception e) {
			response.getWriter().print("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Error teamid!');</script>");
			return;
		}
		EvalHandle handle=new EvalHandle();
		if(handle.addAttendantToEe(eeid, teamid)) {
			response.getWriter().print("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Successfully added!');</script>");
		} else {
			response.getWriter().print("<script>window.location.href='admin_ee_manage.jsp?eeid=" + eeid + "';alert('Fail to add!');</script>");
		}
}

}

