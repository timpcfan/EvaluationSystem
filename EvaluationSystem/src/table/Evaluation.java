package table;

import java.util.List;

/**
 * 评价信息
 */
public class Evaluation {
	private int id; // 主键，评价的id
	private int eeid; // 评价所属的评价活动id
	private String userid; // 进行评价的用户id
	private int teamid; // 被评价的小组id
	private int score = 0; // 评分分值 1~10 ，0表示未评价
	private String remark = ""; // 评价的文本
	private String lastEditTime; // 最后编辑时间（作为排序依据）
	
	// 附加信息
	private String teamName; // 小组名称
	private String ename; // 实验名称
	private String einfo; // 作品介绍
	private List<User> teammates; // 小组成员
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEeid() {
		return eeid;
	}
	public void setEeid(int eeid) {
		this.eeid = eeid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	} 
	public int getTeamid() {
		return teamid;
	}
	public void setTeamid(int teamid) {
		this.teamid = teamid;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getLastEditTime() {
		return lastEditTime;
	}
	public void setLastEditTime(String lastEditTime) {
		this.lastEditTime = lastEditTime;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEinfo() {
		return einfo;
	}
	public void setEinfo(String einfo) {
		this.einfo = einfo;
	}
	public List<User> getTeammates() {
		return teammates;
	}
	public void setTeammates(List<User> teammates) {
		this.teammates = teammates;
	}
	@Override
	public String toString() {
		return String.format("Evaluation[id=%d, eeid=%d, userid=%s, teamid=%d, score=%d, remark=%s, lastEditTime=%s]", 
										id, eeid, userid, teamid, score, remark, lastEditTime);
	}
}
