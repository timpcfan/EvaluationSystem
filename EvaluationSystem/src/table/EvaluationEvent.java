package table;

import java.util.Date;

import common.Helper;

/**
 * 评价活动信息
 */
public class EvaluationEvent {
	// 状态常量字符串
	public static final String INIT = "init";
	public static final String PREPARE = "prepare";
	public static final String AVAILABLE = "available";
	public static final String PAUSE = "pause";
	public static final String STOP = "stop";
	
	
	private int id; // 主键，评价活动id（eeid）
	private String name; // 评价活动名称
	private String info; // 评价活动的介绍
	private String status; // 评价活动状态
	private String startTime; // 开始时间文本
	private String endTime; // 结束时间文本
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	/**
	 * 获取评价活动状态，会根据时间自动判断状态
	 * @return 当前评价活动状态
	 */
	public String getStatus() {
		updateStatus();
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	@Override
	public String toString() {
		return String.format("Ee[id=%d, name=%s, info=%s, status=%s, startTime=%s, endTime=%s]", 
										id, name, info, status, startTime, endTime);
	}
	private void updateStatus() {
		if(status == null) return; // 无状态则不更新
		
		Date now = new Date();
		Date start = Helper.DBTimeToDate(startTime == null || startTime.equals("") || startTime.equalsIgnoreCase("null") ? "20301231235959" : startTime);
		Date end = Helper.DBTimeToDate(endTime == null || endTime.equals("") || endTime.equalsIgnoreCase("null") ? "20301231235959" : endTime);
		
		switch(status) {
		case INIT: // 如果在init状态，不自动更新状态
			break;
		case PREPARE: // 如果在prepare状态，到达开始时间时会自动更新状态为available，到结束时间会更新状态为stop
			if(now.after(start)) status = AVAILABLE;
		case AVAILABLE: // 如果在available状态，到结束时间会更新状态为stop
		case PAUSE: // 如果在pause状态，到结束时间会更新状态为stop
			if(now.after(end))  status = STOP;
			break;
		case STOP: // 如果在stop状态，不自动更新状态
			break;
		default: // 非法阶段置为初始值"init"
			status = INIT;
		}
	}
}
