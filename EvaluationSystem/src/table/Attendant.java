package table;

/**
 * 参与某个评价活动的组为Attendant
 */
public class Attendant{
	private int eeid; // 主键1，评价活动id 
	private int teamid; // 主键2，小组id
	private String teamName; // 小组名称（附加信息）
	private String teamInfo; // 小组介绍（附加信息）
	private String ename; // 实验名称
	private String einfo; // 实验介绍
	private String selfeval; // 自我评价
	public int getEeid() {
		return eeid;
	}
	public Attendant() {
	}
	public Attendant(Team t) {
		teamid = t.getId();
		teamName = t.getName();
		teamInfo = t.getInfo();
	}
	public void setEeid(int eeid) {
		this.eeid = eeid;
	}
	public int getTeamid() {
		return teamid;
	}
	public void setTeamid(int teamid) {
		this.teamid = teamid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEinfo() {
		return einfo;
	}
	public void setEinfo(String einfo) {
		this.einfo = einfo;
	}
	public String getSelfeval() {
		return selfeval;
	}
	public void setSelfeval(String selfeval) {
		this.selfeval = selfeval;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getTeamInfo() {
		return teamInfo;
	}
	public void setTeamInfo(String teamInfo) {
		this.teamInfo = teamInfo;
	}
	/**
	 * 摘取Attendant中的小组信息部分，生成对应的Team对象
	 * @return Team对象
	 */
	public Team toTeam() {
		Team t = new Team();
		t.setId(teamid);
		t.setName(teamName);
		t.setInfo(teamInfo);
		return t;
	}
	@Override
	public String toString() {
		return String.format("Attendant[eeid=%d, teamid=%d, teamName=%s, teamInfo=%s, ename=%s, einfo=%s, selfeval=%s]", 
										eeid, teamid, teamName, teamInfo, ename, einfo, selfeval);
	}
}                                  
                                   