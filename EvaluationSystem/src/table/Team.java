package table;

/**
 * 小组信息
 */
public class Team {
	private int id = 0; // teamid小组编号
	private String name = ""; // 小组名称
	private String info = ""; // 小组介绍
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	@Override
	public String toString() {
		return String.format("Team[id=%d, name=%s, info=%s]", id, name, info);
	}
}
