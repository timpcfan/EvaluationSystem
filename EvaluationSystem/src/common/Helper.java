package common;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 工具类
 */
public class Helper {
	
	/**
	 * 将HTML中的参数（特别是中文字符串）编码转换为UTF-8形式，解决乱码问题
	 * @param s 乱码的字符串
	 * @return
	 */
	public static String getString(String s){
		if(s == null){
			s = "";
		}
			
		try {
			
			System.out.printf("[%s] -> ", s);
			byte b[] = s.getBytes(StandardCharsets.ISO_8859_1);
			
			s = new String(b, StandardCharsets.UTF_8);
			System.out.printf("[%s]\n", s);
			
		}catch(Exception b){}
		
		return s;
	}
	
	/**
	 * 将字符串转换为对应的MD5码，用于密码储存与比对
	 * @param s
	 * @return
	 */
	public static String toMd5(String s) {
        char hexDigits[] = {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
        };
        try {
            byte[] btInput = s.getBytes("utf-8");
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }
	}
	
	/**
	 * 将数据库时间戳格式字符串转为Date对象
	 * @param DBTime
	 * @return Date对象
	 */
	public static Date DBTimeToDate(String DBTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");  
		Date ret = new Date(0);
		try {
			ret = sdf.parse(DBTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 将时间转换为数据库保存的字符串格式
	 * @param date
	 * @return 数据库时间戳
	 */
	public static String dateToDBTime(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}
	
	/**
	 * 将HTML参数里的时间转换为Date
	 * @param dateStr
	 * @return Date对象
	 */
	public static Date parseTimeString(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date ret = new Date(0);
		try {
			ret = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 将时间转为HTML支持的字符串格式
	 * @param date 时间
	 * @return HTML支持的时间字符串
	 */
	public static String dateToHTMLTime(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		return sdf.format(date);
	}

	/**
	 * 将时间转换成可读的字符串
	 * @param date 时间
	 * @return 可读的字符串
	 */
	public static String dateToReadableString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日 HH:mm");
		return sdf.format(date);
	}
	
	/**
	 * 将时间转换成可读的字符串带年份和秒数
	 * @param date 时间
	 * @return 可读的字符串
	 */
	public static String dateToReadableLongString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		return sdf.format(date);
	}
}
