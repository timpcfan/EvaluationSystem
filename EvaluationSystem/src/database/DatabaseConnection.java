package database;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 用于创建数据库连接的工具类
 */
public class DatabaseConnection {
	
	/**
	 * 创建数据库的连接并返回连接对象
	 * @return 数据库的连接对象
	 */
	public static Connection getConnection(){
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://timpcfan.cn:3306/evalsystem?useUnicode=true&characterEncoding=utf-8&useSSL=false";
			conn = DriverManager.getConnection(url, "evalsystem", "te66KXYBADOD4wPT");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
}
