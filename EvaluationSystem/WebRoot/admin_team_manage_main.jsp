<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.*" %>
<%@ page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>小组管理页面</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<script>

function isDelete(uid){
	if(confirm("确认删除？")){
		deleteTeam(uid);
	}
	
}

function deleteTeam(uid){
	//兼容性写法创建请求实例,IE5 6支持else里面的方法
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	//设置传送方式,地址,以及同步还是异步
	xmlhttp.open("GET", "DeleteTeam?teamid=" + escape(uid), true);
	xmlhttp.onreadystatechange = callback;//状态改变的时候执行这个函数,用来判断是否请求完毕
	xmlhttp.send();//请求服务器
}

function isAdd(){
	if(confirm("确认添加？")){
		addNewTeam();
	}
	
}
function addNewTeam(){
	//兼容性写法创建请求实例,IE5 6支持else里面的方法
	
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	//设置传送方式,地址,以及同步还是异步
	xmlhttp.open("GET", "AddTeam", true);
	xmlhttp.onreadystatechange = callback2;//状态改变的时候执行这个函数,用来判断是否请求完毕
	xmlhttp.send();//请求服务器
}

function callback(){
	//请求完成表示
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if (result === "OK") {//这里直接判断不为空,应该根据数据库返回值来进行不同的显示
			alert("删除成功！");
			location.reload();
		} 
		else{
			alert("删除失败！");
		}		
	}
}


function callback2(){
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if(result === "OK"){
			alert("添加成功！请编辑小组信息");
			location.reload();
		}
		else{
			alert("添加失败！已存在id为0的小组，请修改其信息");
		}		
	}
}

</script>


</head>

<body onload="initStyle()">
<%
	// 管理员登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null || !user.getGroup().equals("admin")){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	String teamidScreen = "";
	String pteamidScreen = request.getParameter("teamidScreen");
	if(pteamidScreen != null){
		teamidScreen = pteamidScreen;
	}
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">
  
  <%@ include file="admin_menu.html" %>
  <%-- 这里插入了菜单内容 --%>
    
   
  <div class="content">
    
    <div id="frame">
    <div id="frame_header">
    <!-- 显示筛选部分以及添加小组按钮 -->
      <form action="#" method="get">
        查询小组编号：<input type="text" name="teamidScreen" id="txt_teamid" value="">
        <input type="submit" name="submit" value="查找">
        <input type="button" name="addTeam" value="添加小组" onclick="isAdd()">
      </form>
   <%
    if(!teamidScreen.isEmpty()){
    	// 编号筛选模式下显示筛选编号
    	out.println("<br><h4>ID中包含\"" + teamidScreen + "\"的小组：</h4>");
    }
    %>
	  </div>
    
    <div id="frame_content">
    <!-- 显示小组列表内容 -->
    
    <table class="list" width="100%" border="1">
      <thead>
        <th width="20%">小组ID</th>
        <th width="30%">小组名称</th>
        <th width="20%">成员数量</th>
        <th width="15%">编辑</th>
        <th width="15%">删除</th>
  	  </thead>
     

    
    <div id="frame_content">
    <!-- 显示学生列表内容 -->
    
  
      <%
      		// 输出小组表格
      		List<Team> teams = new ArrayList();
      		TeamHandle handle = new TeamHandle();
      		if(!teamidScreen.isEmpty()){
      		teams.addAll(handle.getTeamsWithIdLike(teamidScreen));
      		}else{ // 如果teamid参数为0则显示所有用户信息
	      		teams = handle.getAllTeams();
      		}
      		for(Team t: teams){
      		    List<User> users = handle.getUsersFromTeam(t.getId());
      	        int member_num = users.size();
      			out.println("<tr>");
      			out.println("<td>" + t.getId() + "</td>");
      			out.println("<td>" + t.getName() + "</td>");
      			if(t.getId()==0) {out.println("<td>" + 0 + "</td>");}
      			else{
      				out.println("<td>" + member_num + "</td>");
      			}
      			out.println("<td><a href=\"admin_team_manage.jsp?teamid=" + t.getId() + "\">编辑</a></td>");
      			out.println("<td><a href=\"#\" onclick=\"isDelete('" + t.getId() + "')\">删除</a></td>");
      			out.println("</tr>");
      		}
      %>
    </table>
    <%
	if(!teamidScreen.isEmpty()){ // 在编号筛选模式下有返回按钮
		out.println("<input type=\"button\" name=\"back\" value=\"返回\" onclick=\"window.history.back(-1);\">");
	}
    %>
    </div>    
    </div>
    
    
    
    <!-- end .content --></div>
    <%@ include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
