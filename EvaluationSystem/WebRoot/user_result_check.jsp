<%@page import="java.util.stream.Collectors"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*" import="table.*" import="common.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
</head>

<body onload="initStyle()">
<%
	// 用户登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	int eeid = Integer.parseInt(request.getParameter("eeid"));
	EvalHandle handle = new EvalHandle();
	List<Attendant> allAttendants = handle.getAttendantsFromEe(eeid);
	List<Evaluation> allEvals = handle.getAllEvalsFromEe(eeid);
	EvaluationEvent ee = handle.getEeById(eeid);
	
	// 总评价人数
	long numOfPeopleWhoEvaluated = allEvals.stream().filter(eval->eval.getScore()!=0).collect(Collectors.groupingBy(Evaluation::getUserid, Collectors.toSet())).size();
	// 评价条数
	long numOfEvaluation = allEvals.stream().filter(eval->eval.getScore()!=0).count();
	// 小组数量
	long numOfAttendant = allAttendants.size();
	
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    
    <div id="frame">
      <div id="frame_header">
      	<h1 align="center">评价结果</h1>
      </div>
      
      <div id="frame_content">
      	<h1 style="center"><%= ee.getName() %></h1>
      	总评人数：<%= numOfPeopleWhoEvaluated %><br>
      	评论条数：<%= numOfEvaluation %><br><br>
      	
      	包含小组：<%= numOfAttendant %><br>
      	<table class="list" width="100%" border="1">
      	  <thead>
          	<th width="15%">小组ID</th>
      	    <th width="20%">组名</th>
      	    <th width="30%">实验名称</th>
      	    <th width="20%">平均成绩</th>
      	    <th width="15%">评价结果</th>
      	  </thead>
      	  <%
      	  	for(int i = 0; i < numOfAttendant; i++){
      	  		
      	  		Attendant att = allAttendants.get(i);
      	  		
      	  		// 平均成绩
      	  		double aveScore = allEvals.stream().filter(eval->eval.getTeamid()==att.getTeamid()).filter(eval->eval.getScore()!=0).collect(Collectors.averagingDouble(Evaluation::getScore));
      	  		
      	  		out.println("<tr>");
      	  		out.println("<td>" + att.getTeamid() + "</td>");
      	  		out.println("<td>" + att.getTeamName() + "</td>");
      	  		out.println("<td>" + (att.getEname()==null?"[未命名]":att.getEname()) + "</td>");
      	  		out.println("<td>" + (Double.compare(aveScore, 0)==0?"无评价":String.format("%.2f", aveScore)) + "</td>");
      	  		out.println("<td>" + "<a href=\"user_evaluation_result.jsp?eeid=" + eeid + "&&teamId=" + att.getTeamid() + "\">" + "进入</td>");
      	  		out.println("</tr>");
      	  	}
      	  %>
      	</table>
      </div>
    </div>
    
    
    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
