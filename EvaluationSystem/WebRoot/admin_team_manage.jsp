<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.*" %>
<%@ page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>小组管理页面</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

</head>


<body onload="initStyle()">
<%
	// 管理员登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null || !user.getGroup().equals("admin")){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	// TODO 健壮性排查！！
    int teamid =0;
	try{
		teamid = Integer.valueOf(request.getParameter("teamid"));
	}catch(Exception e){}
	Team selectedTeam = new Team();
	TeamHandle thandle = new TeamHandle();
	selectedTeam = thandle.getTeamById(teamid);
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">
 
 <%@ include file="admin_menu.html" %>
  <%-- 这里插入了菜单内容 --%>
  
  <div class="content">
    
    <div id="frame">
    <form action="ModifyTeam" method="post">
    <h3>小组基本信息修改</h3>
       小组编号：<input type="text" name="teamid" id="txt_teamid" value="<%= selectedTeam.getId() %>"> <br>
       小组名称：<input type="text" name="team_name" value="<%= selectedTeam.getName() %>"> <br>
	小组简介:<br>
    <textarea name="team_info" rows="6" cols="50" ><%= selectedTeam.getInfo() %></textarea> <br>
    <input type="submit" name="submit" value="提交">
    <input type="reset" name="reset" value="重置">
    <%-- 上面那句表示延迟100ms调用teamChanged() --%>
    <input type="button" name="back" value="返回" onclick="window.location.href='admin_team_manage_main.jsp'">
    <h3>小组成员管理</h3>
    <% 
       List<User> users = thandle.getUsersFromTeam(teamid);
      int member_num =0;
      if(teamid!=0) {
    	  member_num = users.size();
      }
    %>
      成员数量：<label id="lbl_member_num"><%= member_num %></label> 
    <input type="button" name="edit_member" value="管理组员" onclick="window.location.href='admin_user_manage_main.jsp?mode=1&teamid=<%= teamid %>'">  <br>
    <input type="hidden" name="old_teamid" value="<%= teamid %>">
    </form>
    </div>
    
    
    <!-- end .content --></div>
    <%@ include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
