<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.*" %>
<%@ page import="java.util.*" %>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统管理员主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<%
	// 管理员登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null || !user.getGroup().equals("admin")){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	// 初始化表格项数量
	int num_of_items = 10;
	String tmpnum = request.getParameter("n");
	try{ num_of_items = Integer.parseInt(tmpnum);
	}catch(NumberFormatException e){}
	
	int mode = 0;
	String pmode = request.getParameter("mode");
	try{ mode = Integer.parseInt(pmode);
	}catch(NumberFormatException e) {}
	List<User> ulist = new ArrayList<>();
	if(mode == 1){
		ulist = (List<User>) request.getAttribute("userlist");
		num_of_items = ulist.size();
	}
%>


<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}


function teamChanged(no){
	// 小组编号修改完
	var ele = document.getElementById("txt_teamid_" + no);
	var tid = ele.value;
	var reg = /^[0-9]*$/;
	if(tid === ""){
		return;
	}
	if (!reg.test(tid)){
		alert("小组编号必须是整数！");
		ele.value = "";
		return;
	}
	if(tid >= 32767 || tid < 0){
		alert("小组编号范围为1~32766！");
		ele.value = "";
		return;
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", "GetTeamName?teamid=" + escape(tid) + "&no=" + escape(no), true);
	xmlhttp.onreadystatechange = updateTeamName;
	xmlhttp.send();
}

function updateTeamName(){
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var name = xmlhttp.responseXML.getElementsByTagName('teamname')[0].textContent;
		var no = xmlhttp.responseXML.getElementsByTagName('no')[0].textContent;
		var ele = document.getElementById("s_teamname_" + no);
		if(name === "null"){
			ele.innerHTML = "不存在该id对应的小组(可稍后添加)";
			ele.style = "color:red;";
		}else{
			ele.innerHTML = name;
			ele.style = "";
		}
	}
}
</script>
</head>

<body onload="initStyle()">


<div class="container">

  <%@ include file="admin_menu.html" %>
  
  <div class="content">
    
    <div id="frame">
	  <h1>批量用户添加</h1>
    <form action="AddUsers" method="post">
	  <table class="list" width="100%" border="1">
	  	<thead>
		  <th width="20%">用户编号</th>
		  <th width="15%">姓名</th>
		  <th width="15%">密码</th>
		  <th width="12%">小组编号</th>
		  <% if(mode != 1){ %> 
		  <th width="38%">小组名称</th>
		  <% } %>
		</thead>
		<%
		if(mode == 1){
			for(int i=0; i<ulist.size(); i++){
				User u = ulist.get(i);
				out.println("<tr>");
				out.println("<td><input type=\"text\" id=\"txt_userid_" + i + "\" name=\"txt_userid_" + i + "\" style=\"width:100%\" value=\"" + u.getId() + "\"></td>");
				out.println("<td><input type=\"text\" id=\"txt_username_" + i + "\" name=\"txt_username_" + i + "\" style=\"width:100%\" value=\"" + u.getName() +"\"></td>");
				out.println("<td><input type=\"text\" id=\"txt_password_" + i + "\" name=\"txt_password_" + i + "\" style=\"width:100%\" value=\"" + u.getPassword() + "\"></td>");
				out.println("<td><input type=\"text\" id=\"txt_teamid_" + i + "\" name=\"txt_teamid_" + i + "\" style=\"width:100%\" list=\"team_list\" value=\"" + u.getTeamid() + "\"></td>");	
				out.println("</tr>");		
			}
		}else
		for(int i=0; i<num_of_items; i++){
			out.println("<tr>");
			out.println("<td><input type=\"text\" id=\"txt_userid_" + i + "\" name=\"txt_userid_" + i + "\" style=\"width:100%\"></td>");
			out.println("<td><input type=\"text\" id=\"txt_username_" + i + "\" name=\"txt_username_" + i + "\" style=\"width:100%\"></td>");
			out.println("<td><input type=\"text\" id=\"txt_password_" + i + "\" name=\"txt_password_" + i + "\" style=\"width:100%\" value=\"000000\"></td>");
			out.println("<td><input type=\"text\" id=\"txt_teamid_" + i + "\" name=\"txt_teamid_" + i + "\" style=\"width:100%\" onblur=\"teamChanged(" + i + ")\" list=\"team_list\"></td>");
			out.println("<td><span id=\"s_teamname_" + i + "\"></span></td>");
			out.println("</tr>");		
		}
		%>
		</table>
		<br>
		添加模式：
		<label title="如果存在一项id重复则所有都不能添加"><input name="add_mode" type="radio" id="RadioGroup1_0" value="safe" checked>安全模式</label>
  		<label title="添加id不重复的项"><input name="add_mode" type="radio" id="RadioGroup1_1" value="normal">一般模式</label>
  		<label title="如果id重复，则将新的数据更改原有的数据项"><input name="add_mode" type="radio" id="RadioGroup1_2" value="force">强制模式</label>
		<br><br>
		<input type="submit" name="submit" value="提交">
		<span>&nbsp;&nbsp;</span>
		
		<% if(mode != 1) { %>
		<input type="button" name="importfromexcel" value="通过excel文件导入用户" onclick="window.location.href='admin_user_import.jsp'">
		<% } %>
		<input type="hidden" name="num_of_items" value="<%= num_of_items %>">
    </form>
    <datalist id="team_list">
    	<%
    	TeamHandle thandle = new TeamHandle();
    	List<Team> tlist = thandle.getAllTeams();
    	for(Team t: tlist){
    		if(t.getId() == 0) continue;
    		out.println(String.format("<option label=\"%s\" value=\"%d\" />", t.getName(), t.getId()));
    	}
    	%>
    </datalist>
	</div>
    
    
    
  <!-- end .content --></div>
  <%@ include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
