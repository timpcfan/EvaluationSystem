<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*" import="java.util.*"%>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<script>
function check(){
	if(form.txt_team_ename.value == ""){
		alert('小组作品名不能为空！');
		return false;
	}
	if(form.txt_team_production_introduce.value == ""){
		alert('小组作品介绍不能为空！');
		return false;
	}
	if(form.txt_team_self_evaluate.value == ""){
		alert('小组自我评价不能为空！');
		return false;
	}
}

</script>

</head>

<body onload="initStyle()">
<%
	//用户登录有效性验证段
	User user = (User)session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	int eeid = Integer.parseInt(request.getParameter("eeid"));
	EvalHandle handle = new EvalHandle();
	Attendant att = handle.getAttendant(user.getTeamid(), eeid);
	String ename = att.getEname()==null?"":att.getEname();
	String selfeval = att.getSelfeval()==null?"":att.getSelfeval();
	String info = att.getEinfo()==null?"":att.getEinfo();
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    <div id="frame">
    <form action="UserTeamSelfEvaluate?eeid=<%=eeid %>" method="post" name="form" onsubmit="return check()">
           小组作品名：<input type="text" name="txt_team_ename" value="<%=ename%>"><br>
           小组作品介绍：<br>
      <textarea rows="3" cols="60" name="txt_team_production_introduce"><%=info %></textarea><br>
           小组自我评价：<br>
      <textarea rows="3" cols="60" name="txt_team_self_evaluate"><%=selfeval %></textarea><br><br>
      <input type="submit" name="submit" value="提交">
    </form>
    </div>
    
    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
