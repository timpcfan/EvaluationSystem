<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*"%>
<%@page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户小组信息页面</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<script>
function check(){
	if(form1.txt_team_name_modify.value == ""){
		alert('小组名字不能为空！');
		return false;
	}
	if(form1.txt_team_intro_modify.value == ""){
		alert('小组简介不能为空！');
		return false;
	}
}

</script>
</head>

<body onload="initStyle()">
<%
	User user = (User)session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	TeamHandle handle = new TeamHandle();
	Team newTeam = handle.getTeamById(user.getTeamid());
	String name = newTeam.getName()==null?"未填写":newTeam.getName();
	String info = newTeam.getInfo()==""?"未填写":newTeam.getInfo();
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    <div id="frame">
    <form action="TeamInfoModify" method="post" name="form1" onsubmit="return check()">
	    <table align="center">
	    	<tr>
	    		<td align="right">组名：</td>
	    		<td align="left">
	    			<input type="text" name="txt_team_name_modify" value="<%=name %>">
	    		</td>
	    	</tr>
	    	<tr>
	    		<td>小组简介：</td>
	    		<td width=300 align="left">
	    			<textarea  name="txt_team_intro_modify" rows="3" cols="30"><%=info %></textarea>
	    		</td>
	    	</tr>
	    	<tr>
		    	<td colspan="2" align="center">
		    		<!-- 通过提交表单跳转到修改信息页面 -->
		    		<input type="submit" name="submit" value="保存">
		    	</td>
		    </tr>
	    </table>
    </form>
    <input type="button" name="back" value="返回" onclick="window.location.href='user_team_info.jsp'">
    </div>
    
    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
