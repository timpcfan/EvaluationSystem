<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.*"%>
<%@ page import="java.util.*"%>
<%@ page import="common.*"%>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统管理员主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
</head>

<%
// 管理员登录有效性验证段
User user = (User) session.getAttribute("user");
if(user == null || !user.getGroup().equals("admin")){
	request.getRequestDispatcher("index.jsp").forward(request, response);
	return;
}
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<body onload="initStyle();">


<div class="container">

  <%@ include file="admin_menu.html"%>

  <div class="content">
    
    <div id="frame">
      <h1>使用Excel导入用户</h1>
      <p>说明：按照模板将需要导入的用户按格式填写在模板相应位置，再上传即可。<a href="pattern.xls">点击下载模板</a></p>
      <form action="UserExcelUpload" enctype="multipart/form-data" method="post" style="margin:20px;">
      选择上传文件：<input type="file" name="file_upload_excel"><br>
      <input type="submit" name="submit" value="提交">
      </form>
	  </div>
    
    
    
    
    <!-- end .content --></div>
    <%@ include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
