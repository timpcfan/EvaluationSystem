<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*" import="table.*" import="common.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
</head>

<body onload="initStyle()">
<%
	User user = (User)session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	int eeid = Integer.parseInt(request.getParameter("eeid"));
	EvalHandle handle = new EvalHandle();
	List<Evaluation> list = handle.getEvalsOfUserInEe(user.getId(), eeid);
	TeamHandle thandle = new TeamHandle();
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    <div id="frame">
    <form action="UpdateEvaluation?eeid=<%=eeid %>" method="post" onsubmit="return check()">
    	
    	<%
    		for(int i=0; i<list.size(); i++){
    			Evaluation e = list.get(i);
    			out.println("<table class=\"list\" width=\"100%\" border=\"1\">");
    			out.println("<thead><th width=\"20%\">小组ID：</th><th><label id=\"lbl_team_id_" + i + "\">" + e.getTeamid() + "</label></th></thead>");
    			out.println("<tr><td>小组名称：</td><td><label id=\"lbl_team_name_" + i + "\">" + e.getTeamName() + "</label></td></tr>");
    			String uName = "";
    			for(User u:thandle.getUsersFromTeam(e.getTeamid())) uName += u.getName() + " ";
    			out.println("<tr><td>小组成员：</td><td><label id=\"lbl_team_member_" + i + "\">" + uName + "</label></td></tr>");
    			out.println("<tr><td>小组作品：</td><td><label id=\"lbl_team_work_" + i + "\">" + (e.getEname()==null?"[未命名]":e.getEname()) + "</label></td></tr>");
    			out.println("<tr><td>作品介绍：</td><td>" + (e.getEinfo()==null?"[未填写]":e.getEinfo()) + "</td></tr>");
    			String temp1 = "";
    			for(int j=1; j<=10; j++){
    				if(list.get(i).getScore() == j || (list.get(i).getScore()==0 && j==5))
    					temp1 += "<option value=\"" + j + "\" selected>" + j + "</option>";
    				else
    					temp1 += "<option value=\"" + j + "\">" + j + "</option>";
    			}
    			out.println("<tr><td>评分成绩：</td><td><select name=\"num_score_" + i + "\">"+
    							temp1 + "</select></td></tr>");
    			out.println("<tr><td>评语内容：</td><td><textarea rows=\"3\" cols=\"60\" id=\"abc" + i + "\"" +" name=\"txt_eval_" + i + "\">" +
    							(e.getRemark()==null?"":e.getRemark()) + "</textarea></td></tr>");
    			out.println("</table><br>");
    		}
	    %>
	    <input type="submit" name="submit" value="保存">
	    <input type="button" name="back" value="返回" onclick="window.history.back(-1);">
    </form>
  </div>
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
