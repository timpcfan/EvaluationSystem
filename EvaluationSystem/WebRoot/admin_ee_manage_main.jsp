<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.*" %>
<%@ page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统管理员主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
<script>
function isDelete(eeid){
	if(confirm("确认删除？")){
		delete_ee(eeid);
	}
	
}
function delete_ee(eeid){
	//兼容性写法创建请求实例,IE5 6支持else里面的方法
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	//设置传送方式,地址,以及同步还是异步
	xmlhttp.open("GET", "DeleteEe?eeid=" + escape(eeid), true);
	xmlhttp.onreadystatechange = callback;//状态改变的时候执行这个函数,用来判断是否请求完毕
	xmlhttp.send();//请求服务器
}

function callback(){
	//请求完成表示
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if (result === "OK") {//这里直接判断不为空,应该根据数据库返回值来进行不同的显示
			alert("删除成功！");
			location.reload();
		} else {
			alert("删除失败！");
		}
	}
}
function isAdd(){
	if(confirm("确认添加？")){
		add_ee();
	}
	
}
function add_ee(){
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", "AddEe", true);
	xmlhttp.onreadystatechange = callback2;
	xmlhttp.send();
}

function callback2(){
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if (result === "OK") {
			alert("添加成功，请在页面最下方查看")
			location.reload(); // 成功则直接刷新页面（这时会在页面中显示新的评论活动）
		} else {
			alert("添加失败！");
		}
	}
}
</script>
</head>

<body onload="initStyle();">
<%
	// 管理员登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null || !user.getGroup().equals("admin")){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	// 获取查询eeid字段
	String eeidScreen = request.getParameter("eeidScreen");
	
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@ include file="admin_menu.html" %>

  <div class="content">
    
   <div id="frame">
    <div id="frame_header">
    <!-- 显示筛选部分以及添加学生按钮 -->
      <form action="#" method="get">
        查询评价活动编号：<input type="text" name="eeidScreen" value="">
        <input type="submit" name="submit" value="查找">
        <input type="button" name="addEe" value="添加评论活动"  onclick="isAdd()">
      </form>
      
	  </div>    
    <div id="frame_content">
    <!-- 显示学生列表内容 -->
       <%
    if(eeidScreen != null && !eeidScreen.isEmpty()){
    	// 编号筛选模式下显示筛选编号
    	out.println("<br><h4>ID中包含\"" + eeidScreen + "\"的评价活动：</h4>");
    }
    %>
    <table class="list" width="100%" border="1">
      <thead>
        <th width="10%">评价活动编号</th>
        <th width="40%">评价活动名称</th>
        <th width="20%">状态</th>
        <th width="15%">管理</th>
        <th width="15%">删除</th>
  	  
  	  <%
      		// 输出用户表格
      		EvalHandle handle = new EvalHandle();
      		List<EvaluationEvent> ee = null;
  	  		if(eeidScreen != null){
  	  			ee = handle.getEesWithIdLike(eeidScreen);
  	  		} else {
  	  			ee = handle.getAllEe();
  	  		}
      		for(EvaluationEvent e: ee){
      			out.println("<tr>");
      			out.println("<td>" + e.getId() + "</td>");
      			out.println("<td>" + e.getName() + "</td>");
      			out.println("<td>" + e.getStatus() + "</td>");
      			out.println("<td><a href=\"admin_ee_manage.jsp?eeid=" +e.getId()+ "\">管理</a></td>");
      			out.println("<td><a href=\"#\" onclick=\"isDelete('" + e.getId() + "')\">删除</a></td>");
      			out.println("</tr>");
      		}
      %>
      </thead>
    </table>
    <%
	if(eeidScreen != null && !eeidScreen.isEmpty()){ // 在编号筛选模式下有返回按钮
		out.println("<input type=\"button\" name=\"back\" value=\"返回\" onclick=\"window.history.back(-1);\">");
	}
    %>
    </div>    
    </div>
    
    <!-- end .content --></div>
    <%@ include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
