<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*" %>
<%@page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<%
	// 用户登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	UserHandle uh = new UserHandle();
	uh.updateInfoById(user);
%>


<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}

function check(){
	if(form1.txt_account.value == ""){
		alert("请输入ID!");
		return false;
	}
	if(form1.txt_name.value == ""){
		alert("请输入姓名!");
		return false;
	}
	
	<%
	if(user.getTeamid() == 0){
	%>
		if(form1.txt_team.value != ""){
			if(!confirm("确定提交？只有一次修改小组的机会"))
				return false;
		
			var tid = form1.txt_team.value
			var reg = /^[0-9]+$/;
			if (!reg.test(tid)){
				alert("小组编号必须是整数！");
				return false;
			}
		}
	<%
	}
	%>
	
}
</script>
</head>

<body onload="initStyle()">

<div class="container">

  <%@ include file="user_menu.html" %>
  <div class="content">
    <div id="frame">
    <form action="SelfModifyInfo" method="post" name="form1" onsubmit="return check()">
    	<table align="center">
    	<tr>
    		<td align="right">帐号：</td>
			<td align="left">
				<input type="text" name="txt_account" value="<%=user.getId()%>">
			</td>
		</tr>
		<tr>
			<td align="right">姓名：</td>
			<td align="left">
				<input type="text" name="txt_name" value="<%=user.getName()%>">
			</td>
		</tr>
		<tr>
			<td align="right">小组ID：</td>
			<td align="left">
				<input type="text" name="txt_team" list="team_list" value="<%=user.getTeamid() == 0 ? "" : user.getTeamid()%>" title="注：只有未分配小组的情况下才可以自行修改小组，若已经分配小组，请联系管理员修改。" <%= user.getTeamid() == 0 ? "" : "disabled" %>>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input type="submit" name="submit" value="提交">
            </td>
    	</tr>
    	</table>
    </form>
    <datalist id="team_list">
    	<%
    	TeamHandle thandle = new TeamHandle();
    	List<Team> tlist = thandle.getAllTeams();
    	for(Team t: tlist){
    		if(t.getId() == 0) continue;
    		out.println(String.format("<option label=\"%s\" value=\"%d\" />", t.getName(), t.getId()));
    	}
    	%>
    </datalist>
    <form action="SelfModifyPSW" method="post">
    	<table align="center">
    	<tr>
    		<td align="right">密码修改：</td>
			<td align="left">
				<input type="password" name="txt_user_password" value="">
			</td>
		</tr>
		<tr>
			<td align="right">确认密码：</td>
			<td align="left">
				<input type="password" name="txt_user_password_confirm" value="">
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input type="submit" name="submit" value="提交">
            </td>
    	</tr>
    	</table>
    </form>
    <input type="button" name="back" value="返回" onclick="window.location.href='user_self_info.jsp'">
    </div>
    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
