<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*" import="table.*" import="java.util.stream.*"%>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统管理员主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
</head>

<body onload="initStyle()">
<%
	// 用户登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	int eeid = Integer.parseInt(request.getParameter("eeid"));
	int teamId = Integer.parseInt(request.getParameter("teamId"));
	EvalHandle handle = new EvalHandle();
	Attendant att = handle.getAttendant(teamId, eeid);
	TeamHandle thandle = new TeamHandle();
	List<Evaluation> evals = handle.getEvalsForTeamInEe(teamId, eeid);
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    
   <div id="frame">
    <div id="frame_header">
    <!-- 显示筛选部分以及添加学生按钮 -->
	  <h1 align="center">评价结果</h1>
	</div>
    
    <div id="frame_content">
    <!-- 显示学生列表内容 -->
    <table border="1" width="100%" class="list">
       <thead>
       	<th width="20%">小组ID</th>
		<th><%= att.getTeamid() %></th>
	   </thead>
       <tr>
       	<td>小组名</td>
		<td><%= att.getTeamName() %></td>
	   </tr>
        <%
	        List<User> temp = thandle.getUsersFromTeam(teamId);
			String uName = "";
			for(User u:temp)
				uName = uName + u.getName() + "  ";
        %>
       <tr>
       	<td>小组成员</td>
		<td><%= uName %></td>
	   </tr>
	   <tr>
       	<td>实验名称</td>
		<td><%= att.getEname()==null?"[未设置]":att.getEname() %></td>
	   </tr>
        <%
        double aveScore = evals.stream().filter(eval->eval.getScore()!=0).collect(Collectors.averagingDouble(Evaluation::getScore));
      	%>
       <tr>
       	<td>实验成绩</td>
		<td><%= (Double.compare(aveScore, 0)==0?"无评价":String.format("%.2f", aveScore)) %></td>
	   </tr>
   	   <tr>    
        <td>评论建议</td>
		<td>
			<%
				int count = 1;
				for(Evaluation eval: evals){
					if(eval.getRemark()!=null&&eval.getRemark().equals("")){
						continue;
					}
					out.println(count + "：" + eval.getRemark() + "<br>");
					count++;
				}
			%>
		</td>
	   </tr>
	</table>
	<br>
	<input type="button" value="返回" onclick="window.history.back(-1);">
    </div>    
    </div>
    
    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
