<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*" %>
<%@page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
</head>

<body onload="initStyle()">
<%
	// 用户登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<%
	UserHandle handle = new UserHandle();
	handle.updateInfoById(user);
%>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    <div id="frame">
    <table align="center">
    <tr>
    	<td align="right">帐号：</td>
		<td align="left">
			<%= user.getId() %>
		</td>
	</tr>
	<tr>
		<td align="right">姓名：</td>
		<td align="left">
			<%= user.getName() %>
		</td>
	</tr>
	<tr>
		<td align="right">所属小组：</td>
		<td align="left">
			<%
				if(user.getTeamid() == 0)
					out.println("[未分配小组]");
				else
					out.println(user.getTeamName());
			%>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<!-- 通过提交表单来修改个人信息和密码 -->
			<form action="user_self_info_modify.jsp" method="post">
				<input type="submit" name="submit" value="编辑">
			</form>
		</td>
   	</tr>
   	</table>
	</div>

    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
