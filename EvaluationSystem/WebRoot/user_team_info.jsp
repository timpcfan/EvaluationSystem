<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*" %>
<%@page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户小组信息页面</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
</head>

<body onload="initStyle()">
<%
	// 用户登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	UserHandle handle = new UserHandle();
	handle.updateInfoById(user);
	TeamHandle thandle = new TeamHandle();
	Team team = thandle.getTeamById(user.getTeamid());
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    <div id="frame">
    <!-- 用if判断是否有小组，有则显示情况一，没有就显示情况二 -->
    <!-- 情况一 -->
 <%
    if(user.getTeamid() != 0){
%>
    <table border="1" align="center">
    	<tr>
    		<td align="right">小组编号：</td>
    		<td align="left">
    			<%=team.getId() %><!-- 显示小组id -->
    		</td><br>
    	</tr>
    	<tr>
    		<td align="right">小组名称：</td>
    		<td align="left">
    			<%=team.getName() %><!-- 显示组名 -->
    		</td><br>
    	</tr>
    	<tr>
    		<td>小组简介：</td>
    		<td width="300" align="left">
    			<%=team.getInfo() %><!-- 显示小组简介信息 -->
    		</td>
    	</tr>
    	<tr>
	    	<td colspan="2" align="center">
	    		<!-- 通过提交表单跳转到修改信息页面 -->
	    		<form action="user_team_info_modify.jsp" method="post">
	    			<input type="submit" name="submit" value="编辑">
	    		</form>
	    	</td>
	    </tr>
    </table>
    <% }else{ %>
    <!-- 情况二 -->
    您尚未加入小组，点击返回键在个人信息页面加入小组<br>
    <input type="button" name="back" onclick="window.location.href='user_self_info_modify.jsp'" value="返回">
    <%} %>
    </div>
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
