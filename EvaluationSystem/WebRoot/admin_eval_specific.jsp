<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*" %>
<%@page import="java.util.*" %>
<%@page import="common.Helper" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>评价明细信息</title>
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<script>
function deleteEval(evalid){
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", "DeleteEval?evalid=" + escape(evalid), true);
	xmlhttp.onreadystatechange = callback;
	xmlhttp.send();
}

function callback(){
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if (result === "OK") {
			alert("删除成功！");
			location.reload();
		} else {
			alert("删除失败！");
		}
	}
}
</script>

</head>
<body>
<%
// 管理员登录有效性验证段
User user = (User) session.getAttribute("user");
if(user == null || !user.getGroup().equals("admin")){
	request.getRequestDispatcher("index.jsp").forward(request, response);
	return;
}

String seeid = request.getParameter("eeid");
int eeid = 0;
try { eeid = Integer.parseInt(seeid); } 
catch(Exception e){
	out.println("<h1 style=\"color:red;\">参数错误！！</h1>");
	return;
}

EvalHandle eh = new EvalHandle();
UserHandle uh = new UserHandle();
TeamHandle th = new TeamHandle();
List<Evaluation> list = eh.getAllEvalsFromEe(eeid);

%>
<div id="frame">
<h1>评价活动ID：<%= eeid %></h1>
<table class="list" border="1">
      <thead>
        <th>最后修改时间</th>
        <th>评价者用户ID</th>
        <th>评价者名称</th>
        <th>评价者所属小组ID</th>
        <th>评价者所属小组名称</th>
        <th>被评价的小组ID</th>
        <th>被评价的小组名称</th>
        <th>分数</th>
        <th>文本评价内容</th>
        <th>删除</th>
      </thead>

<%

for(Evaluation e : list){
	String lastEditTime = Helper.dateToReadableLongString(Helper.DBTimeToDate(e.getLastEditTime()));
	String uid = e.getUserid();
	User u = uh.getUserById(uid);
	if(u == null)
		u = new User();
	String uname = "[用户已被删除]";
	if(u.getName() != null)
		uname = u.getName();
	String stid1 = "N/A";
	if(u.getTeamid() != 0)
		stid1 = "" + u.getTeamid();
	String tname1 = "[小组已被删除]";
	if(u.getTeamName() != null && !u.getTeamName().equalsIgnoreCase("null"))
		tname1 = u.getTeamName();
	int tid2 = e.getTeamid();
	String stid2 = tid2 + "";
	Team t = th.getTeamById(tid2);
	String tname2 = "[小组已被删除]";
	if(t.getName() != null)
		tname2 = t.getName();
	String score = String.valueOf(e.getScore());
	String remark = "";
	if(e.getRemark() != null && !e.getRemark().equalsIgnoreCase("null"))
		remark = e.getRemark();
	String deleteLink = "<a href=\"#\" onclick=\"deleteEval(" + e.getId() + ");return false;\">删除</a>";

	String line = String.format(
			"<td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>",
			lastEditTime, uid, uname, stid1, tname1, stid2, tname2, score, remark, deleteLink);
	out.println("<tr>" + line + "</tr>");
}
%>
</table>
<p>目前评价总数：<%= list.size() %>
</div>
</body>
</html>