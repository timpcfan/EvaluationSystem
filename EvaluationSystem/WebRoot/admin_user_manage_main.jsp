<%@page import="table.TeamHandle"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.User" %>
<%@ page import="table.UserHandle"%>
<%@ page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>用户管理页面</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<%
	// 管理员登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null || !user.getGroup().equals("admin")){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	// 检测模式
	// 有0和1两种模式
	// 模式0表示，处于常规模式，编辑用户功能正常使用，删除功能表示从数据库中完全删除用户
	// 模式1表示，处于组成员管理模式，无编辑用户功能，删除功能表示将用户从小组中删除
	int currentMode = 0;
	String pmode = request.getParameter("mode");
	try{ currentMode = Integer.parseInt(pmode);
	} catch(Exception e){}
	if(currentMode != 0 && currentMode != 1){
		out.print("<script>window.history.back(-1);alert('Invalid mode');</script>");
		return;
	}
	
	// 检测组筛选
	// 组筛选有两种形式
	// teamid为0表示显示所有组（即显示所有用户）
	// teamid为其他数值表示显示对应teamid的小组的所有成员
	int teamid = 0;
	String pteamid = request.getParameter("teamid");
	try{ teamid = Integer.parseInt(pteamid);
	} catch(Exception e){}
	
	if(currentMode == 1 && teamid == 0){ // mode=1时必须有teamid才可以
		out.print("<script>window.history.back(-1);alert('Invalid parameters');</script>");
		return;
	}
	
	// 用户筛选参数设置
	String useridScreen = "";
	String puseridScreen = request.getParameter("useridScreen");
	if(puseridScreen != null){
		useridScreen = puseridScreen;
	}
%>

<script>

function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}

function isDelete(tid){
	if(confirm("确认删除？")){
		deleteUser(tid);
	}
	
}

function deleteUser(uid){
	//兼容性写法创建请求实例,IE5 6支持else里面的方法
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	//设置传送方式,地址,以及同步还是异步
	xmlhttp.open("GET", "DeleteUser<%= currentMode == 0 ? "" : "FromTeam" %>?userid=" + escape(uid), true);
	<%-- 上面语句表示，若处于mode0则访问DeleteUser控制器，若处于mode1则访问DeleteUserFromTeam控制器 --%>
	xmlhttp.onreadystatechange = callback;//状态改变的时候执行这个函数,用来判断是否请求完毕
	xmlhttp.send();//请求服务器
}

function callback(){
	//请求完成表示5
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if (result === "OK") {//这里直接判断不为空,应该根据数据库返回值来进行不同的显示
			alert("删除成功！");
			location.reload();
		} else {
			alert("删除失败！");
		}
	}
}

function addUserToTeam(tid){
	// 向小组添加成员
	var uid = document.getElementById("txt_userid").value;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", "AddUserToTeam?userid=" + escape(uid) + "&teamid=" + escape(tid), true);
	<%-- 上面语句表示，若处于mode0则访问DeleteUser控制器，若处于mode1则访问DeleteUserFromTeam控制器 --%>
	xmlhttp.onreadystatechange = callback2;
	xmlhttp.send();
}

function callback2(){
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var result = xmlhttp.responseXML.getElementsByTagName('result')[0].textContent;
		if (result === "OK") {
			alert("添加成功！");
			location.reload();
		} else {
			alert("添加失败！");
		}
	}
}
</script>


</head>

<body onload="initStyle()">

<div class="container">
  <%@ include file="admin_menu.html" %>
    
  <div class="content">
    
    <div id="frame">
    <div id="frame_header">
    <!-- 显示筛选部分以及添加学生按钮 -->
    
    <%
    if(currentMode == 1){ // 在组成员模式下显示当前小组的ID
    	out.println("<h3>小组编号：" + teamid + "</h3>");
    } 
    %>
    
    <form action="#" method="get">
    <%
    if(currentMode == 0){
    %>
        查询编号：<input type="text" name="useridScreen" id="txt_userid" value="">
    <input type="submit" name="submit" value="查找">
    <input type="button" name="addUser" value="添加用户" onclick="window.location.href='admin_user_add.jsp'">
    <%
    } else { 
    %>
        向小组添加用户：<input type="text" name="txt_userid" id="txt_userid" list="user_list" value="" placeholder="此处输入用户ID">
    <input type="button" name="addUser" value="确定" onclick="addUserToTeam(<%= teamid %>)">
	<%
    }
	%>
    </form>
    
    <%
    if(!useridScreen.isEmpty()){
    	// 成员编号筛选模式下显示筛选编号
    	out.println("<br><h4>ID中包含\"" + useridScreen + "\"的用户：</h4>");
    }
    %>
    
   	<%
   	if(currentMode == 1){
   		// 如果mode为1则加载添加用户提示列表
   		out.println("<datalist id=\"user_list\">");
    	UserHandle uhandle = new UserHandle();
    	List<User> ulist = uhandle.getAllUsers();
    	for(User u: ulist){
    		out.println(String.format("<option label=\"%s\" value=\"%s\" />", u.getName(), u.getId()));
    	}    		
    	out.println("</datalist>");
   	}
   	%>
    
	</div>
    
    <div id="frame_content">
    <!-- 显示学生列表内容 -->
    
    <table class="list" width="100%" border="1">
      <thead>
        <th width="30%">用户ID</th>
        <th width="20%">姓名</th>
        <%
        	if(currentMode == 0){ // 组成员管理模式下无编辑功能
        %>
        <th width="20%">所属小组</th>
        <th width="15%">编辑</th>
        <%
        	}
        %>
        <th width="15%">删除</th>
      </thead>
      <%
      		// 输出用户表格
      		List<User> users = new ArrayList<>();
      		if(!useridScreen.isEmpty()){
      			UserHandle uhandle = new UserHandle();
      			users.addAll(uhandle.getUsersWithIdLike(useridScreen));
      		}else if(teamid != 0){ // 如果teamid参数不为0，则显示对应teamid的成员
      			TeamHandle thandle = new TeamHandle();
      			users = thandle.getUsersFromTeam(teamid);
      		}else{ // 如果teamid参数为0则显示所有用户信息
	      		UserHandle uhandle = new UserHandle();
	      		users = uhandle.getAllUsers();
      		}
      		for(User u: users){
      			out.println("<tr>");
      			out.println("<td>" + u.getId() + "</td>");
      			out.println("<td>" + u.getName() + "</td>");
      			if(currentMode == 0){ // 组成员管理模式下无编辑功能
	      			if(u.getTeamid() == 0){
		      			out.println("<td>[未分配小组]</td>");
	      			}else{
		      			out.println("<td>" + u.getTeamName() + "</td>");
	      			}
      				out.println("<td><a href=\"admin_user_manage.jsp?userid=" + u.getId() + "\">编辑</a></td>");
      			}
      			out.println("<td><a href=\"#\" onclick=\"isDelete('" + u.getId() + "')\">删除</a></td>");
      			out.println("</tr>");
      		}
      %>
    </table>
	<%
	if(currentMode == 1 || !useridScreen.isEmpty()){ // 在组成员管理模式或用户编号筛选模式下有返回按钮
		out.println("<input type=\"button\" name=\"back\" value=\"返回\" onclick=\"window.history.back(-1);\">");
	}
	%>
    </div>    
    </div>
    <!-- end .content --></div>
    
	<%@ include file="footer.html" %>
    
    
  <!-- end .container --></div>
</body>
</html>
