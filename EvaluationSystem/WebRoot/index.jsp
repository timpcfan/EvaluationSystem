<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.User" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统登录</title>
<style>
#loginbox {
	border:2px solid;
	width:400px;
	height:200px;
	text-align:center;
	padding:30px;
	margin:auto;
}

#title {
	font-size:20px;
	font-weight:bold;
}

</style>
</head>

<body>
<%
	// 登录状态检测
	User user = (User) session.getAttribute("user");
	
	// 跳转到相应的登录页面
	if(user != null){
		if("admin".equals(user.getGroup()))
			request.getRequestDispatcher("admin_main.jsp").forward(request, response);
		else if("user".equals(user.getGroup()))
			request.getRequestDispatcher("user_main.jsp").forward(request, response);
		else
			request.getRequestDispatcher("Logout").forward(request, response);
			// 如果没有对应的组就退出重新登录
	}
%>



<div id="loginbox">
<form action="LoginCheck" method="post">
<p id="title">同行评价系统用户登录</p>

<dl>
<dt>账号：<input type="text" name="txt_username" value=""></dt>
<dt>密码：<input type="password" name="txt_password" value=""> </dt>
</dl>

<p>
  <label><input name="user_type" type="radio" id="RadioGroup1_0" value="user" checked>用户</label>
  <label><input name="user_type" type="radio" id="RadioGroup1_1" value="admin">管理员</label>
  <label><input name="user_type" type="radio" id="RadioGroup1_2" value="guest">访客</label>
</p>

<p>
  <input type="submit" name="btn_login" id="btn_login" value="登录">
  <input type="reset" name="btn_reset" id="btn_reset" value="重置">
</p>

</form>
</div>
</body>
</html>
