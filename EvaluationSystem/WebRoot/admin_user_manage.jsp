<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="table.*" %>
<%@ page import="java.util.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>用户信息管理页面</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<%
	// 管理员登录有效性验证段
	User user = (User) session.getAttribute("user");
	if(user == null || !user.getGroup().equals("admin")){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}

	String userid = request.getParameter("userid");
	User selectedUser = new User();
	selectedUser.setId(userid);
	UserHandle uhandle = new UserHandle();
	uhandle.updateInfoById(selectedUser);
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}

function teamEditing(){
	// 小组编号被修改
	var tid = document.getElementById("txt_teamid").value;
	var reg = /^[0-9]+$/;
	if (!reg.test(tid)) return;
	
	var ele = document.getElementById("s_team_name");
	ele.innerHTML = "查询中...";
	ele.style = "";
}

function teamChanged(){
	// 小组编号修改完
	var tid = document.getElementById("txt_teamid").value;
	var reg = /^[0-9]+$/;
	if (!reg.test(tid)){
		alert("小组编号必须是整数！");
		return;
	}
	if (tid == 0) {
		document.getElementById("s_team_name").innerHTML = '[无小组]';
		return;
	}
	
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", "GetTeamName?teamid=" + escape(tid), true);
	xmlhttp.onreadystatechange = updateTeamName;
	xmlhttp.send();
}

function updateTeamName(){
	if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
		var name = xmlhttp.responseXML.getElementsByTagName('teamname')[0].textContent;
		var ele = document.getElementById("s_team_name");
		if(name === "null" || name === ""){
			ele.innerHTML = "不存在该id对应的小组(可稍后添加)";
			ele.style = "color:red;";
		}else{
			ele.innerHTML = name;
			ele.style = "";
		}
	}
}

function checkAvailable(){
	var ret = <%= userid.equals(user.getId()) ? "false" : "true" %>;
	if(!ret) alert('不能解除自己管理员身份！');
	return ret;
}
</script>
</head>

<body onload="initStyle();teamChanged();">

<div class="container">

  <%@ include file="admin_menu.html" %>
  
  <div class="content">
    
    <div id="frame">
    <form action="ModifyUser" method="post">
	用户编号：<input type="text" name="userid" value="<%= selectedUser.getId() %>"> <br>
	用户姓名：<input type="text" name="user_name" value="<%= selectedUser.getName() %>"> <br>
	小组编号：<input type="text" name="teamid" id="txt_teamid" list="team_list" onblur="teamChanged();" onchange="teamEditing();teamChanged();" value="<%= selectedUser.getTeamid() %>"> <br>
	小组名称：<span id="s_team_name"><%= selectedUser.getTeamName() %></span> <br>
	修改密码：<input type="text" name="reset_password" value=""> 注：留空为不修改 <br>
    <input type="submit" name="submit" value="提交">
    <input type="reset" name="reset" onclick="setTimeout(&quot;teamChanged()&quot;,&quot;100&quot;);" value="重置">
    <%-- 上面那句表示延迟100ms调用teamChanged() --%>
    <input type="button" name="back" value="返回" onclick="window.location.href='admin_user_manage_main.jsp'">
    <input type="hidden" name="old_userid" value="<%= userid %>">
    </form>
    <br>
    
	<form action="GrantAdmin" method="post">
	<input type="hidden" name="userid" value="<%= selectedUser.getId() %>"/>
	<% if(selectedUser.getGroup().equals("admin")) { %>
	权限：管理员<br>
	<input type="hidden" name="setTo" value="user"/>
	<input type="submit" name="submit" value="解除管理员身份" onclick="return checkAvailable();"/>
	<% } %>
	<% if(selectedUser.getGroup().equals("user")) { %>
	权限：普通用户<br>
	<input type="hidden" name="setTo" value="admin"/>
	<input type="submit" name="submit" value="升级为管理员"/>
	<% } %>
	</form>
    
    
    <datalist id="team_list">
    	<%
    	TeamHandle thandle = new TeamHandle();
    	List<Team> tlist = thandle.getAllTeams();
    	for(Team t: tlist){
    		if(t.getId() == 0) continue;
    		out.println(String.format("<option label=\"%s\" value=\"%d\" />", t.getName(), t.getId()));
    	}
    	out.println("<option label=\"[无小组]\" value=\"0\"");
    	%>
    </datalist>
    
    </div>
    
    <!-- end .content --></div>
    
    <%@ include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
