<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="table.*" import="java.util.*" import="common.*"%>
<%@page import="java.lang.*" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>同行评价系统用户主页</title>
<link href="css/twoColLiqLt.css" rel="stylesheet" type="text/css">
<link href="css/tableStyles.css" rel="stylesheet" type="text/css">
<link href="css/frameStyles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->

<script>
function check(){
	//
	if(form1.txt_eeid.value == ""){
		alert('编号不能为空');
		return false;
	}
	
	var tid = form1.txt_eeid.value
	var reg = /^[0-9]+$/;
	if (!reg.test(tid)){
		alert("编号必须是整数！");
		return false;
	}
}

</script>

</head>

<body onload="initStyle()">

<%
	//用户登录有效性验证段
	User user = (User)session.getAttribute("user");
	if(user == null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	
	EvalHandle handle = new EvalHandle();
	List<EvaluationEvent> list = handle.getEesForTeam(user.getTeamid());
%>

<script>
function initStyle(){
	var ele = document.getElementById("login_text");
	ele.innerText = "登录信息：<%= user.getName() %> (<%= user.getId() %>)";
}
</script>

<div class="container">

  <%@include file="user_menu.html" %>
  <div class="content">
    
    <div id="frame">
    	<div id="frame_header">
   	 	<!-- 显示筛选部分以及添加学生按钮 -->
      		<form action="user_find_evaluation.jsp" method="post" name="form1" onsubmit="return check()">
        	查询评价活动编号：<input type="text" name="txt_eeid" value="">
        		<input type="submit" name="submit" value="查找">
      		</form>
	  	</div>
	  	
	  	<div id="frame_content">
		<!-- 显示评价活动列表内容 -->
		    
		    <table class="list" width="100%" border="1">
		      <thead>
		        <th width="10%">评价活动编号</th>
		        <th width="40%">评价活动名称</th>
		        <th width="15%">状态</th>
		        <th width="35%">时间</th>
		        <th width="13%">操作</th>
		  	  </thead>
			  <%
				for(int i=0; i<list.size(); i++){
					String stime = list.get(i).getStartTime();
					String etime = list.get(i).getEndTime();
					String startTime = null;
					String endTime = null;
					if(stime != null)
						startTime = Helper.dateToReadableString(Helper.DBTimeToDate(stime));
					if(etime != null)
						endTime = Helper.dateToReadableString(Helper.DBTimeToDate(etime));
					String output = "<td>" + (stime == null? "未确定" : startTime) + " 至 " + (etime == null? "未确定" : endTime) + "</td>";
					out.println("<tr>");
					out.println("<td>" + list.get(i).getId() + "</td>");
					out.println("<td>" + list.get(i).getName() + "</td>");
					if(EvaluationEvent.INIT.equals(list.get(i).getStatus())){
						out.println("<td>未开始</td>");
						out.println(output);
						out.println("<td></td>");
					}else if(EvaluationEvent.PREPARE.equals(list.get(i).getStatus())){
						out.println("<td>" + "准备" + "</td>");
						out.println(output);
						out.println("<td><a href=\"user_team_info_change.jsp?eeid=" + list.get(i).getId() + "\">修改信息</a></td>");
					}else if(EvaluationEvent.AVAILABLE.equals(list.get(i).getStatus())){
						out.println("<td>" + "进行中" + "</td>");
						out.println(output);
						out.println("<td><a href=\"user_edit_evaluation.jsp?eeid=" + list.get(i).getId() + "\">进入评价</a></td>");
					}else if(EvaluationEvent.PAUSE.equals(list.get(i).getStatus())){
						out.println("<td>" + "暂停" + "</td>");
						out.println(output);
						out.println("<td></td>");
					}else if(EvaluationEvent.STOP.equals(list.get(i).getStatus())){
						out.println("<td>" + "已结束" + "</td>");
						out.println(output);
						out.println("<td><a href=\"user_result_check.jsp?eeid=" + list.get(i).getId() + "\">查看结果</a></td>");
					}else{
						out.println("<td></td>");
						out.println("<td></td>");
						out.println("<td></td>");
					}
					out.println("</tr>");
				}
			  %>
		    </table>
		
		    </div>
    </div>
    
    
    <!-- end .content --></div>
    <%@include file="footer.html" %>
  <!-- end .container --></div>
</body>
</html>
